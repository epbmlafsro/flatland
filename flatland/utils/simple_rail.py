from typing import Tuple

import numpy as np

from flatland.core.grid.rail_env_grid import RailEnvTransitions
from flatland.core.transition_map import GridTransitionMap


def make_custom_rail() -> Tuple[GridTransitionMap, np.array]:
    """
    In this method we create a custom railway grid according to the specifications of our model.
    Returns a GridTransitionMap "rail" and an array "rail_map", which can be used by a rail_generator
    """
    transitions = RailEnvTransitions()
    cells = transitions.transition_list

    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    right_turn_from_south = cells[8]
    right_turn_from_west = transitions.rotate_transition(right_turn_from_south, 90)
    right_turn_from_north = transitions.rotate_transition(right_turn_from_south, 180)
    right_turn_from_east = transitions.rotate_transition(right_turn_from_south, 270)
    simple_switch_north_left = cells[2]
    simple_switch_north_right = cells[10]
    simple_switch_east_left = transitions.rotate_transition(simple_switch_north_left, 90)
    simple_switch_east_right = transitions.rotate_transition(simple_switch_north_right, 90)
    simple_switch_west_left = transitions.rotate_transition(simple_switch_north_left, 270)
    simple_switch_west_right = transitions.rotate_transition(simple_switch_north_right, 270)
    simple_switch_south_left = transitions.rotate_transition(simple_switch_north_left, 180)
    simple_switch_south_right = transitions.rotate_transition(simple_switch_north_right, 180)
    double_switch_north_west = cells[5]
    double_switch_north_east = transitions.rotate_transition(double_switch_north_west, 90)

    rail_map = np.array(
        [[empty] * 80] * 5 +
        [[empty] * 27 + [dead_end_from_south] + [empty] * 52] +
        [[empty] * 27 + [vertical_straight] + [empty] * 52] +
        [[empty] * 27 + [vertical_straight] + [empty] * 9 + [dead_end_from_east] + [horizontal_straight] + [
            simple_switch_west_left] + [horizontal_straight] * 2 + [dead_end_from_west] + [empty] * 24 + [
             dead_end_from_east] + [horizontal_straight] + [simple_switch_west_left] + [horizontal_straight] * 3 + [
             simple_switch_east_right] + [horizontal_straight] + [dead_end_from_west] + [empty] * 4] +
        [[empty] + [dead_end_from_east] + [horizontal_straight] * 4 + [simple_switch_east_right] + [
            horizontal_straight] * 4 + [
             simple_switch_west_left] + [horizontal_straight] * 8 + [simple_switch_west_left] + [
             horizontal_straight] * 6 + [
             simple_switch_west_right] + [simple_switch_east_right] + [horizontal_straight] * 8 + [
             simple_switch_east_right] + [
             simple_switch_west_left] + [simple_switch_east_left] + [horizontal_straight] * 15 + [
             simple_switch_west_left] + [
             horizontal_straight] * 12 + [simple_switch_west_left] + [simple_switch_east_left] + [
             horizontal_straight] * 3 + [
             simple_switch_west_right] + [simple_switch_east_right] + [horizontal_straight] * 2 + [
             dead_end_from_west] + [
             empty] * 2] +
        [[empty] + [dead_end_from_east] + [horizontal_straight] * 4 + [double_switch_north_east] + [
            simple_switch_west_left] + [
             horizontal_straight] * 3 + [simple_switch_east_left] + [horizontal_straight] * 7 + [
             simple_switch_west_left] + [
             simple_switch_east_left] + [horizontal_straight] * 7 + [simple_switch_west_right] + [
             horizontal_straight] * 8 + [
             simple_switch_west_right] + [simple_switch_east_left] + [simple_switch_east_right] + [
             horizontal_straight] * 14 + [
             simple_switch_west_left] + [simple_switch_east_left] + [horizontal_straight] * 12 + [
             simple_switch_east_left] + [
             horizontal_straight] * 5 + [simple_switch_west_right] + [horizontal_straight] * 2 + [
             dead_end_from_west] + [
             empty] * 2] +
        [[empty] + [dead_end_from_east] + [horizontal_straight] * 4 + [simple_switch_west_right] + [
            simple_switch_east_left] + [
             horizontal_straight] * 3 + [right_turn_from_west] + [empty] * 7 + [vertical_straight] + [empty] * 16 + [
             dead_end_from_east] + [horizontal_straight] + [simple_switch_east_right] + [simple_switch_north_left] + [
             empty] * 5 + [right_turn_from_south] + [horizontal_straight] * 6 + [right_turn_from_west] + [empty] + [
             simple_switch_north_right] + [simple_switch_west_left] + [horizontal_straight] + [dead_end_from_west] + [
             empty] * 22] +
        [[empty] + [dead_end_from_east] + [horizontal_straight] * 5 + [simple_switch_east_left] + [
            horizontal_straight] * 2 + [
             simple_switch_east_right] + [double_switch_north_east] + [horizontal_straight] * 4 + [
             simple_switch_west_left] + [
             horizontal_straight] * 2 + [simple_switch_east_left] + [horizontal_straight] + [dead_end_from_west] + [
             empty] * 16 + [vertical_straight] * 2 + [empty] * 2 + [dead_end_from_east] + [horizontal_straight] + [
             simple_switch_west_left] + [simple_switch_east_left] + [horizontal_straight] * 6 + [
             simple_switch_west_right] + [
             simple_switch_west_left] + [right_turn_from_north] + [simple_switch_south_left] + [horizontal_straight] + [
             dead_end_from_west] + [empty] * 22] +
        [[empty] * 7 + [dead_end_from_east] + [horizontal_straight] + [right_turn_from_west] + [
            vertical_straight] * 2 + [
             empty] * 4 + [vertical_straight] + [empty] * 21 + [vertical_straight] + [simple_switch_south_left] + [
             horizontal_straight] * 4 + [simple_switch_east_left] + [horizontal_straight] * 7 + [
             simple_switch_west_left] + [
             right_turn_from_north] + [empty] + [vertical_straight] + [empty] * 24] +
        [[empty] + [dead_end_from_east] + [horizontal_straight] * 7 + [double_switch_north_east] + [
            simple_switch_west_right] + [double_switch_north_east] + [horizontal_straight] * 3 + [
             simple_switch_west_left] + [
             right_turn_from_north] + [empty] * 21 + [vertical_straight] + [right_turn_from_east] + [
             simple_switch_east_right] + [horizontal_straight] * 10 + [simple_switch_west_left] + [
             right_turn_from_north] + [
             empty] * 2 + [vertical_straight] + [empty] * 24] +
        [[empty] * 7 + [dead_end_from_east] + [horizontal_straight] + [double_switch_north_east] + [
            horizontal_straight] + [
             simple_switch_west_right] + [horizontal_straight] * 2 + [simple_switch_west_left] + [
             right_turn_from_north] + [
             empty] * 20 + [dead_end_from_east] + [horizontal_straight] + [simple_switch_west_right] + [
             horizontal_straight] + [
             simple_switch_west_right] + [simple_switch_east_right] + [horizontal_straight] * 9 + [
             double_switch_north_west] + [
             horizontal_straight] * 3 + [simple_switch_south_right] + [empty] * 24] +
        [[empty] * 7 + [dead_end_from_east] + [horizontal_straight] + [simple_switch_west_right] + [
            horizontal_straight] * 4 + [
             simple_switch_east_left] + [horizontal_straight] + [dead_end_from_west] + [empty] * 24 + [
             right_turn_from_east] + [
             simple_switch_east_right] + [horizontal_straight] * 7 + [simple_switch_west_left] + [
             right_turn_from_north] + [
             empty] * 3 + [vertical_straight] + [empty] * 24] +
        [[empty] * 42 + [right_turn_from_east] + [simple_switch_east_right] + [horizontal_straight] * 5 + [
            simple_switch_west_left] + [right_turn_from_north] + [empty] * 4 + [dead_end_from_north] + [empty] * 24] +
        [[empty] * 43 + [right_turn_from_east] + [simple_switch_east_right] + [horizontal_straight] * 3 + [
            simple_switch_west_left] + [right_turn_from_north] + [empty] * 30] +
        [[empty] * 44 + [right_turn_from_east] + [horizontal_straight] * 2 + [simple_switch_west_left] + [
            right_turn_from_north] + [empty] * 31] +
        [[empty] * 45 + [dead_end_from_east] + [horizontal_straight] + [right_turn_from_north] + [empty] * 32] +
        [[empty] * 80] * 7, dtype=np.uint16)

    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(12, 11), (13, 46), (8, 71)]
    train_stations = [[((8, 15), 0), ((9, 15), 1), ((11, 14), 2), ((13, 13), 3), ((15, 12), 4)], [
        ((7, 41), 0), ((8, 47), 1), ((9, 47), 2), ((12, 41), 3), ((13, 46), 4)], [
                          ((7, 71), 0), ((8, 71), 1), ((9, 71), 2)]]
    city_orientations = [1, 1, 3]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                    }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals

def make_simple_rail() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 7x10 grid:
    # Note that that cells have invalid RailEnvTransitions!
    #        |
    #        |
    #        |
    #    _ _ _ _\ _ _  _  _ _ _
    #                /
    #                |
    #                |
    #                |
    transitions = RailEnvTransitions()
    cells = transitions.transition_list
    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    simple_switch_north_left = cells[2]
    simple_switch_north_right = cells[10]
    simple_switch_east_west_north = transitions.rotate_transition(simple_switch_north_right, 270)
    simple_switch_east_west_south = transitions.rotate_transition(simple_switch_north_left, 270)
    rail_map = np.array(
        [[empty] * 3 + [dead_end_from_south] + [empty] * 6] +
        [[empty] * 3 + [vertical_straight] + [empty] * 6] * 2 +
        [[dead_end_from_east] + [horizontal_straight] * 2 +
         [simple_switch_east_west_north] +
         [horizontal_straight] * 2 + [simple_switch_east_west_south] +
         [horizontal_straight] * 2 + [dead_end_from_west]] +
        [[empty] * 6 + [vertical_straight] + [empty] * 3] * 2 +
        [[empty] * 6 + [dead_end_from_north] + [empty] * 3], dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(0,3), (6, 6)]
    train_stations = [
                      [( (0, 3), 0 ) ],
                      [( (6, 6), 0 ) ],
                     ]
    city_orientations = [0, 2]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals


def make_double_track() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 10x18 grid:
    # Note that that cells have invalid RailEnvTransitions!
    #
    #
    #    _ _ _ _ _ _ _ _ _ _
    #       / \     / \
    #    _ _ _ _ _ _ _ _ _ _
    #
    #
    transitions = RailEnvTransitions()
    cells = transitions.transition_list
    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    simple_switch_north_left = cells[2]
    simple_switch_north_right = cells[10]
    simple_switch_west_right = transitions.rotate_transition(simple_switch_north_right, 270)
    simple_switch_west_left = transitions.rotate_transition(simple_switch_north_left, 270)
    simple_switch_east_right = transitions.rotate_transition(simple_switch_north_right, 90)
    simple_switch_east_left = transitions.rotate_transition(simple_switch_north_left, 90)
    rail_map = np.array(
        [[empty] * 18] * 4 +
        [[dead_end_from_east] + [horizontal_straight] * 4 + [simple_switch_west_left] + [simple_switch_east_right] +
         [horizontal_straight] * 4 + [simple_switch_west_left] + [simple_switch_east_right] + [horizontal_straight] * 4 +
         [dead_end_from_west]] +
        [[dead_end_from_east] + [horizontal_straight] * 4 + [simple_switch_east_left] + [simple_switch_west_right] +
         [horizontal_straight] * 4 + [simple_switch_east_left] + [simple_switch_west_right] + [horizontal_straight] * 4 +
         [dead_end_from_west]] +
        [[empty] * 18] * 4, dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(5, 0), (5, 17)]
    train_stations = [
                      [((4, 1), 0), ((5, 1), 1)],
                      [((4, 16), 0), ((5, 16), 1)],
                     ]
    city_orientations = [1, 3]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals


def make_disconnected_simple_rail() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 7x10 grid:
    # Note that that cells have invalid RailEnvTransitions!
    #        |
    #        |
    #        |
    # _ _ _ _\ _    _  _ _ _
    #                /
    #                |
    #                |
    #                |

    transitions = RailEnvTransitions()
    cells = transitions.transition_list
    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    simple_switch_north_left = cells[2]
    simple_switch_north_right = cells[10]
    simple_switch_east_west_north = transitions.rotate_transition(simple_switch_north_right, 270)
    simple_switch_east_west_south = transitions.rotate_transition(simple_switch_north_left, 270)
    rail_map = np.array(
        [[empty] * 3 + [dead_end_from_south] + [empty] * 6] +
        [[empty] * 3 + [vertical_straight] + [empty] * 6] * 2 +
        [[dead_end_from_east] + [horizontal_straight] * 2 +
         [simple_switch_east_west_north] +
         [dead_end_from_west] + [dead_end_from_east] + [simple_switch_east_west_south] +
         [horizontal_straight] * 2 + [dead_end_from_west]] +
        [[empty] * 6 + [vertical_straight] + [empty] * 3] * 2 +
        [[empty] * 6 + [dead_end_from_north] + [empty] * 3], dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(0,3), (6, 6)]
    train_stations = [
                      [( (0, 3), 0 ) ],
                      [( (6, 6), 0 ) ],
                     ]
    city_orientations = [0, 2]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals


def make_simple_rail2() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 7x10 grid:
    #        |
    #        |
    #        |
    # _ _ _ _\ _ _  _  _ _ _
    #               \
    #                |
    #                |
    #                |
    transitions = RailEnvTransitions()
    cells = transitions.transition_list
    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    simple_switch_north_right = cells[10]
    simple_switch_east_west_north = transitions.rotate_transition(simple_switch_north_right, 270)
    simple_switch_west_east_south = transitions.rotate_transition(simple_switch_north_right, 90)
    rail_map = np.array(
        [[empty] * 3 + [dead_end_from_south] + [empty] * 6] +
        [[empty] * 3 + [vertical_straight] + [empty] * 6] * 2 +
        [[dead_end_from_east] + [horizontal_straight] * 2 +
         [simple_switch_east_west_north] +
         [horizontal_straight] * 2 + [simple_switch_west_east_south] +
         [horizontal_straight] * 2 + [dead_end_from_west]] +
        [[empty] * 6 + [vertical_straight] + [empty] * 3] * 2 +
        [[empty] * 6 + [dead_end_from_north] + [empty] * 3], dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(0,3), (6, 6)]
    train_stations = [
                      [( (0, 3), 0 ) ],
                      [( (6, 6), 0 ) ],
                     ]
    city_orientations = [0, 2]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals


def make_simple_rail_unconnected() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 7x10 grid:
    # Note that that cells have invalid RailEnvTransitions!
    #        |
    #        |
    #        |
    # _ _ _  _ _ _  _  _ _ _
    #                /
    #                |
    #                |
    #                |
    transitions = RailEnvTransitions()
    cells = transitions.transition_list
    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    simple_switch_north_left = cells[2]
    # simple_switch_north_right = cells[10]
    # simple_switch_east_west_north = transitions.rotate_transition(simple_switch_north_right, 270)
    simple_switch_east_west_south = transitions.rotate_transition(simple_switch_north_left, 270)
    rail_map = np.array(
        [[empty] * 3 + [dead_end_from_south] + [empty] * 6] +
        [[empty] * 3 + [vertical_straight] + [empty] * 6] +
        [[empty] * 3 + [dead_end_from_north] + [empty] * 6] +
        [[dead_end_from_east] + [horizontal_straight] * 5 + [simple_switch_east_west_south] +
         [horizontal_straight] * 2 + [dead_end_from_west]] +
        [[empty] * 6 + [vertical_straight] + [empty] * 3] * 2 +
        [[empty] * 6 + [dead_end_from_north] + [empty] * 3], dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(0,3), (6, 6)]
    train_stations = [
                      [( (0, 3), 0 ) ],
                      [( (6, 6), 0 ) ],
                     ]
    city_orientations = [0, 2]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals


def make_simple_rail_with_alternatives() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 7x10 grid:
    #  0 1 2 3 4 5 6 7 8 9  10
    # 0        /-------------\
    # 1        |             |
    # 2        |             |
    # 3 _ _ _ /_  _ _        |
    # 4              \   ___ /
    # 5               |/
    # 6               |
    # 7               |
    transitions = RailEnvTransitions()
    cells = transitions.transition_list

    empty = cells[0]
    dead_end_from_south = cells[7]
    right_turn_from_south = cells[8]
    right_turn_from_west = transitions.rotate_transition(right_turn_from_south, 90)
    right_turn_from_north = transitions.rotate_transition(right_turn_from_south, 180)
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    simple_switch_north_left = cells[2]
    simple_switch_north_right = cells[10]
    simple_switch_left_east = transitions.rotate_transition(simple_switch_north_left, 90)
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    double_switch_south_horizontal_straight = horizontal_straight + cells[6]
    double_switch_north_horizontal_straight = transitions.rotate_transition(
        double_switch_south_horizontal_straight, 180)
    rail_map = np.array(
        [[empty] * 3 + [right_turn_from_south] + [horizontal_straight] * 5 + [right_turn_from_west]] +
        [[empty] * 3 + [vertical_straight] + [empty] * 5 + [vertical_straight]] * 2 +
        [[dead_end_from_east] + [horizontal_straight] * 2 + [simple_switch_left_east] + [horizontal_straight] * 2 + [
            right_turn_from_west] + [empty] * 2 + [vertical_straight]] +
        [[empty] * 6 + [simple_switch_north_right] + [horizontal_straight] * 2 + [right_turn_from_north]] +
        [[empty] * 6 + [vertical_straight] + [empty] * 3] +
        [[empty] * 6 + [dead_end_from_north] + [empty] * 3], dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(0,3), (6, 6)]
    train_stations = [
                      [( (0, 3), 0 ) ],
                      [( (6, 6), 0 ) ],
                     ]
    city_orientations = [0, 2]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals



def make_invalid_simple_rail() -> Tuple[GridTransitionMap, np.array]:
    # We instantiate a very simple rail network on a 7x10 grid:
    #        |
    #        |
    #        |
    # _ _ _ /_\ _ _  _  _ _ _
    #               \ /
    #                |
    #                |
    #                |
    transitions = RailEnvTransitions()
    cells = transitions.transition_list

    empty = cells[0]
    dead_end_from_south = cells[7]
    dead_end_from_west = transitions.rotate_transition(dead_end_from_south, 90)
    dead_end_from_north = transitions.rotate_transition(dead_end_from_south, 180)
    dead_end_from_east = transitions.rotate_transition(dead_end_from_south, 270)
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    double_switch_south_horizontal_straight = horizontal_straight + cells[6]
    double_switch_north_horizontal_straight = transitions.rotate_transition(
        double_switch_south_horizontal_straight, 180)
    rail_map = np.array(
        [[empty] * 3 + [dead_end_from_south] + [empty] * 6] +
        [[empty] * 3 + [vertical_straight] + [empty] * 6] * 2 +
        [[dead_end_from_east] + [horizontal_straight] * 2 +
         [double_switch_north_horizontal_straight] +
         [horizontal_straight] * 2 + [double_switch_south_horizontal_straight] +
         [horizontal_straight] * 2 + [dead_end_from_west]] +
        [[empty] * 6 + [vertical_straight] + [empty] * 3] * 2 +
        [[empty] * 6 + [dead_end_from_north] + [empty] * 3], dtype=np.uint16)
    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(0,3), (6, 6)]
    train_stations = [
                      [( (0, 3), 0 ) ],
                      [( (6, 6), 0 ) ],
                     ]
    city_orientations = [0, 2]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                   }
    optionals = {'agents_hints': agents_hints}
    return rail, rail_map, optionals

def make_oval_rail() -> Tuple[GridTransitionMap, np.array]:
    transitions = RailEnvTransitions()
    cells = transitions.transition_list

    empty = cells[0]
    vertical_straight = cells[1]
    horizontal_straight = transitions.rotate_transition(vertical_straight, 90)
    right_turn_from_south = cells[8]
    right_turn_from_west = transitions.rotate_transition(right_turn_from_south, 90)
    right_turn_from_north = transitions.rotate_transition(right_turn_from_south, 180)
    right_turn_from_east = transitions.rotate_transition(right_turn_from_south, 270)

    rail_map = np.array(
        [[empty] * 9] +
        [[empty] + [right_turn_from_south] + [horizontal_straight] * 5 + [right_turn_from_west] + [empty]] +
        [[empty] + [vertical_straight] + [empty] * 5 + [vertical_straight] + [empty]]+
        [[empty] + [vertical_straight] + [empty] * 5 + [vertical_straight] + [empty]] +
        [[empty] + [right_turn_from_east] + [horizontal_straight] * 5 + [right_turn_from_north] + [empty]] +
        [[empty] * 9], dtype=np.uint16)

    rail = GridTransitionMap(width=rail_map.shape[1],
                             height=rail_map.shape[0], transitions=transitions)
    rail.grid = rail_map
    city_positions = [(1, 4), (4, 4)]
    train_stations = [
        [((1, 4), 0)],
        [((4, 4), 0)],
    ]
    city_orientations = [1, 3]
    agents_hints = {'city_positions': city_positions,
                    'train_stations': train_stations,
                    'city_orientations': city_orientations
                    }
    optionals = {'agents_hints': agents_hints}
    return  rail, rail_map, optionals
